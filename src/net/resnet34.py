from src.common_lib import *


class Resnet34(nn.Module):
    def __init__(self, in_channel=4, in_features=2048, n_classes=28):
        super(Resnet34, self).__init__()
        self.model = resnet34(pretrained=True)
        self.in_features = in_features
        # 重みの学習をするように
        for param in self.model.parameters():
            param.requires_grad = True
        self.model.conv1 = Conv2d(in_channel, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        if in_features == 2048:
            self.model.fc = nn.Linear(in_features=in_features,
                                      out_features=int(in_features/8))
            self.model.fc2 = nn.Linear(in_features=int(in_features/8),
                                       out_features=n_classes)
        if in_features == 512:
            self.model.fc = nn.Linear(in_features=in_features,
                                      out_features=int(in_features/4))
            self.model.fc2 = nn.Linear(in_features=int(in_features/4),
                                       out_features=n_classes)
        # self.model.half()
        self.new_layers = [self.model.fc, self.model.fc2]


    def forward(self, x):
        x = self.model(x)
        x = F.dropout(x, training=self.model.training)
        x = F.elu(x)
        x = self.model.fc2(x)
        return x

    def change_freeze(self, is_freeze):
        for param in self.model.parameters():
            param.requires_grad = (not is_freeze)

        # set True which layer of newly constructed
        for layer in self.new_layers:
            for param in layer.parameters():
                param.requires_grad = True

