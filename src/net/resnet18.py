from src.common_lib import *


class Resnet18(nn.Module):
    def __init__(self, in_features=2048, n_classes=28):
        super(Resnet18, self).__init__()
        self.model = resnet18(pretrained=True)

        # 重みの学習をするように
        for param in self.model.parameters():
            self.model.requires_grad = True
        self.model.fc = nn.Linear(in_features=in_features,
                                  out_features=n_classes)

    def forward(self, x):
        x = self.model(x)
        return x