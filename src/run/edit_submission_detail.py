import numpy as np
import pandas as pd
import pickle
from src.common_lib import *

detail_dir = "../../submit/20181216_512*512ext/20181215_141508_fold0/20epoch_h+v+hv/"
with open("{}/submission_detail.numpy.pickle".format(detail_dir), "rb") as f:
    sub_pred = pickle.load(f)
with open("{}/../val_metrics.pickle".format(detail_dir), "rb") as f:
    val_metrics = pickle.load(f)

sub_pred = np.array(sub_pred)

sub_pred = np.percentile(sub_pred, 70, axis=0)
# sub_pred = np.max(sub_pred, axis=0)

df_sub = pd.read_csv("../../data/original/sample_submission.csv")

sub = []
for i in range(len(val_metrics.result)):
    w_sub = (sub_pred[:, i] > val_metrics.result.iloc[i]["threshold"]).astype(int)
    sub.append(w_sub)

sub = np.array(sub).transpose((1, 0))

def make_submission_file(sample_submission_df, predictions):
    submissions = []
    for row in predictions:
        subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
        submissions.append(subrow)

    sample_submission_df['Predicted'] = submissions
    sample_submission_df.to_csv('submission.csv', index=None)

make_submission_file(df_sub, sub)