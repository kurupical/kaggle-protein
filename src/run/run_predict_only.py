from src.net.resnet34 import Resnet34
from src.net.resnet101 import Resnet101
from src.common_lib import *

def run(shape, model_name, epoch, batch_size, step_size, gamma, channel_mode, lr, is_debug=False):

    # https://github.com/creafz/pytorch-cnn-finetune
    seed = 0
    random.seed(seed)
    torch.manual_seed(seed)

    n_splits = 5
    n_classes = 28
    ###########################################
    # pre_process
    ###########################################
    output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d_%H%M%S"))

    # config logger
    os.makedirs(output_dir)
    logger = getLogger("run.py")
    formatter = Formatter("%(asctime)s :%(message)s")

    s_handler = StreamHandler()
    s_handler.setLevel(DEBUG)
    s_handler.setFormatter(formatter)
    f_handler = FileHandler(filename="{}/log.text".format(output_dir))
    f_handler.setLevel(INFO)
    f_handler.setFormatter(formatter)
    logger.setLevel(DEBUG)
    logger.addHandler(s_handler)
    logger.addHandler(f_handler)

    # torch config
    torch.backends.cudnn.benchmark = True


    builded_model = make_model(model_name, num_classes=n_classes, pretrained=True, dropout_p=0.5)
    if channel_mode == "rgby":
        if model_name == "inception_v4":
            builded_model._features[0].conv = Conv2d(4, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name == "pnasnet5large":
            builded_model._features.conv_0.conv = Conv2d(4, 96, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name == "polynet":
            builded_model._features[0].conv1[0].conv = Conv2d(4, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name in ['se_resnet50', 'se_resnet101', 'se_resnet152', 'se_resnext101_32x4d', 'se_resnext50_32x4d',
                          'se_resnext101_64x4d', 'resnet34']:
            builded_model._features[0].conv1 = Conv2d(4, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3),
                                                      bias=False)
    input_dir = "../../submit/20190102_512*512ext_v2/20190101_142002_fold0"
    builded_model.cuda()
    builded_model.eval()
    model = Model(builded_model)
    model.model.load_state_dict(torch.load("{}/best_2_epoch20.pth".format(input_dir)))

    with open("{}/val_metrics.pickle".format(input_dir), "rb") as f:
        val_metrics = pickle.load(f)
    ###########################################
    # submit
    ###########################################

    def tta(df, composed, enable_dropout=False):
        sub_dataset = ProteinDataset(df_images=df,
                                     is_train=False,
                                     shape=shape,
                                     channel_mode=channel_mode,
                                     transform=composed)

        logger.debug("prediction...")
        pred = model.predict(sub_dataset, enable_dropout=enable_dropout)
        return pred
    logger.debug("Submit start")

    df_sub = pd.read_csv("../../data/original/sample_submission.csv")
    if is_debug:
        df_sub = df_sub.iloc[:100]

    sub_pred = []
    # TTA
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.HorizontalFlip(p=1)]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.VerticalFlip(p=1)]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.VerticalFlip(p=1),
                                                         albumentations.HorizontalFlip(p=1)]),
                        enable_dropout=False))
    """
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([Rotate90(p=1)]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([Rotate270(p=1)]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.RandomCrop(384, 384),
                                                         albumentations.Resize(512, 512)]),
                        enable_dropout=False))
    """
    with open("{}/submission_detail.numpy.pickle".format(output_dir), "wb") as f:
        pickle.dump(sub_pred, f)

    sub_pred = np.array(sub_pred).max(axis=0)
    logger.debug("make submit file")
    sub = []
    for i in range(len(val_metrics.result)):
        w_sub = (sub_pred[:, i] > val_metrics.result.iloc[i]["threshold"]).astype(int)
        sub.append(w_sub)

    sub = np.array(sub).transpose((1, 0))

    def make_submission_file(sample_submission_df, predictions):
        submissions = []
        for row in predictions:
            subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
            submissions.append(subrow)

        sample_submission_df['Predicted'] = submissions
        sample_submission_df.to_csv('{}/submission.csv'.format(output_dir), index=None)

    make_submission_file(df_sub, sub)

    s_handler.close()
    f_handler.close()
    logger.handlers = []
    del logger, s_handler, f_handler, formatter
    gc.collect()

if __name__ == "__main__":
    run(shape=512, epoch=30, batch_size=8, model_name="se_resnext50_32x4d", is_debug=False, step_size=4, gamma=0.1, lr=0.0005, channel_mode="rgby")
    # run(shape=256, epoch=20, batch_size=32, model_name="se_resnext50_32x4d", is_debug=False, step_size=3, gamma=0.1, channel_mode="rgby")
    # run(shape=256, epoch=20, batch_size=32, model_name="se_resnext50_32x4d", is_debug=False, step_size=3, gamma=0.1, channel_mode="rgb")
    # run(shape=256, epoch=20, batch_size=32, model_name="se_resnext50_32x4d", is_debug=False, step_size=3, gamma=0.1, channel_mode="rgby")
    # run(shape=256, epoch=20, batch_size=16, model_name="se_resnext50_32x4d", is_debug=False, step_size=5, gamma=0.1)
    # run(shape=256, epoch=30, batch_size=16, model_name="se_resnext101_32x4d", is_debug=False)
