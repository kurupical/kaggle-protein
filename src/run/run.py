from src.net.resnet34 import Resnet34
from src.net.resnet101 import Resnet101
from src.common_lib import *

def run():

    seed = 0
    random.seed(seed)
    torch.manual_seed(seed)

    n_splits = 5
    batch_size = 16
    epoch_1st = 2
    epoch_2nd = 60
    shape = 256
    in_features = int(2048 / (512/shape)**2)
    channel_mode = "rgby"
    is_debug = False
    ###########################################
    # pre_process
    ###########################################
    output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d_%H%M%S"))


    # config logger
    os.makedirs(output_dir)
    logger = getLogger("run.py")
    formatter = Formatter("%(asctime)s :%(message)s")

    s_handler = StreamHandler()
    s_handler.setLevel(DEBUG)
    s_handler.setFormatter(formatter)
    f_handler = FileHandler(filename="{}/log.text".format(output_dir))
    f_handler.setLevel(INFO)
    f_handler.setFormatter(formatter)
    logger.setLevel(DEBUG)
    logger.addHandler(s_handler)
    logger.addHandler(f_handler)

    # torch config
    torch.backends.cudnn.benchmark = True
    ###########################################
    # training
    ###########################################
    logger.debug("Training start")
    df = pd.read_csv("../../data/original/train.csv")
    if is_debug:
        logger.debug("DEBUG MODE")
        df = df.iloc[:50] # test
        epoch_1st = 10
        epoch_2nd = 10
    fold = KFold(n_splits=n_splits, random_state=seed, shuffle=True)


    for train_idx, test_idx in fold.split(df):
        break

    df_train = df.iloc[train_idx]
    df_test = df.iloc[test_idx]

    resnet = Resnet34(in_features=in_features)
    resnet.cuda()
    composed = transforms.Compose([])

    train_dataset = ProteinDataset(df_images=df_train,
                                   is_train=True,
                                   channel_mode=channel_mode,
                                   shape=shape,
                                   transform=composed)
    validation_dataset = ProteinDataset(df_images=df_test,
                                        is_train=True,
                                        shape=shape,
                                        channel_mode=channel_mode)

    train_dataloader = DataLoader(train_dataset,
                                  batch_size=batch_size,
                                  shuffle=True)
    validation_dataloader = DataLoader(validation_dataset,
                                       batch_size=batch_size,
                                       shuffle=False)

    model = Model(resnet)
    # ----------------------
    # 1. Training new layer
    # ----------------------

    logger.debug("** 1. Training new layer ** ")
    # loss = nn.BCEWithLogitsLoss()
    loss = FocalLoss(gamma=2)
    optimizer = optim.Adam(resnet.parameters(), lr=0.001)
    scheduler = ReduceLROnPlateau(optimizer=optimizer, mode="min", factor=0.5, patience=5, verbose=True)
    metrics = F_score()
    checkpoint = Checkpoint(filepath="{}/best_1.pth".format(output_dir),
                            monitor="metrics_val",
                            mode="max",
                            logger=logger)

    model.model.change_freeze(is_freeze=True)
    model.fit(train_dataloader=train_dataloader,
              validation_dataloader=validation_dataloader,
              epoch=epoch_1st,
              loss=loss,
              optimizer=optimizer,
              scheduler=scheduler,
              metrics=metrics,
              logger=logger,
              callbacks=[checkpoint])
    # ----------------------
    # 2. Training all layer
    # ----------------------

    logger.debug("** 2. Training all layer ** ")
    model.model.load_state_dict(torch.load("{}/best_1.pth".format(output_dir)))
    # loss = nn.BCEWithLogitsLoss()
    loss = FocalLoss(gamma=2)
    optimizer = optim.Adam(resnet.parameters(), lr=0.001)
    scheduler = ReduceLROnPlateau(optimizer=optimizer, mode="min", factor=0.5, patience=5, verbose=True)
    metrics = F_score()
    checkpoint = Checkpoint(filepath="{}/best_2.pth".format(output_dir),
                            monitor="metrics_val",
                            mode="max",
                            logger=logger)
    model.model.change_freeze(is_freeze=False)
    model.fit(train_dataloader=train_dataloader,
              validation_dataloader=validation_dataloader,
              epoch=epoch_2nd,
              loss=loss,
              optimizer=optimizer,
              scheduler=scheduler,
              metrics=metrics,
              logger=logger,
              callbacks=[checkpoint])

    # ----------------------
    # 3. Calculate best threshold
    # ----------------------
    _, val_metrics = model.evaluate(validation_dataloader, loss_func=loss, metrics=metrics)
    with open("{}/val_metrics.pickle".format(output_dir), "wb") as f:
        pickle.dump(val_metrics, f)
    logger.info("best threshold: \n{}".format(val_metrics.result))
    ###########################################
    # submit
    ###########################################
    model.model.load_state_dict(torch.load("{}/best_2.pth".format(output_dir)))
    logger.debug("Submit start")
    df_sub = pd.read_csv("../../data/original/sample_submission.csv")
    if is_debug:
        df_sub = df_sub.iloc[:300]
    sub_dataset = ProteinDataset(df_images=df_sub,
                                 is_train=False,
                                 shape=shape,
                                 channel_mode=channel_mode)

    logger.debug("prediction...")
    sub_pred = model.predict(sub_dataset)

    logger.debug("make submit file")
    sub = []
    for i in range(len(val_metrics.result)):
        w_sub = (sub_pred[:, i] > val_metrics.result.iloc[i]["threshold"]).astype(int)
        sub.append(w_sub)

    sub = np.array(sub).transpose((1, 0))

    def make_submission_file(sample_submission_df, predictions):
        submissions = []
        for row in predictions:
            subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
            submissions.append(subrow)

        sample_submission_df['Predicted'] = submissions
        sample_submission_df.to_csv('{}/submission.csv'.format(output_dir), index=None)

    make_submission_file(df_sub, sub)

if __name__ == "__main__":
    # print("sleep...")
    # time.sleep(60*60*1)
    run()
