from src.net.resnet34 import Resnet34
from src.net.resnet101 import Resnet101
from src.common_lib import *

def run(shape, model_name, epoch_1st, epoch_2nd, batch_size, step_size, gamma, channel_mode,
        optimizer, optimizer_param, composed_ary, is_debug=False, first_filter=7, first_padding=3,
        use_external_data=True, use_random_sampler=True, random_sampler_gamma=2.0, target_fold=0, n_splits=5,
        aug_minor_class=False, dropout=0.5, cycliclr_param={}):

    # https://github.com/creafz/pytorch-cnn-finetune
    seed = 0
    random.seed(seed)
    torch.manual_seed(seed)
    np.random.seed(seed)

    n_classes = 28
    ###########################################
    # pre_process
    ###########################################
    output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d_%H%M%S"))


    # config logger
    os.makedirs(output_dir)
    logger = getLogger("run.py")
    formatter = Formatter("%(asctime)s :%(message)s")

    s_handler = StreamHandler()
    s_handler.setLevel(DEBUG)
    s_handler.setFormatter(formatter)
    f_handler = FileHandler(filename="{}/log.text".format(output_dir))
    f_handler.setLevel(INFO)
    f_handler.setFormatter(formatter)
    logger.setLevel(DEBUG)
    logger.addHandler(s_handler)
    logger.addHandler(f_handler)

    # torch config
    torch.backends.cudnn.benchmark = True
    ###########################################
    # training
    ###########################################
    logger.debug("Training start")
    df = pd.read_csv("../../data/original/train.csv")
    if use_external_data:
        df = df.append(pd.read_csv("../../data/external/HPAv18/HPAv18RBGY_wodpl_smallclass.csv"))
    if is_debug:
        logger.debug("DEBUG MODE")
        df = df.iloc[:60] # test
        epoch_1st = 10
        epoch_2nd = 10
    fold = KFold(n_splits=n_splits, random_state=seed, shuffle=True)


    i = 0
    for train_idx, test_idx in fold.split(df):
        if target_fold == i:
            break
        i += 1

    df_train = df.iloc[train_idx]
    df_test = df.iloc[test_idx]

    if aug_minor_class:
        df_train_org = df_train.copy()
        lows = [15, 15, 15]
        for i in lows:
            target = str(i)
            indicies = df_train_org.loc[df_train_org['Target'] == target].index
            df_train = pd.concat([df_train, df_train_org.loc[indicies]], ignore_index=True)
            indicies = df_train_org.loc[df_train_org['Target'].str.startswith(target + " ")].index
            df_train = pd.concat([df_train, df_train_org.loc[indicies]], ignore_index=True)
            indicies = df_train_org.loc[df_train_org['Target'].str.endswith(" " + target)].index
            df_train = pd.concat([df_train, df_train_org.loc[indicies]], ignore_index=True)
            indicies = df_train_org.loc[df_train_org['Target'].str.contains(" " + target + " ")].index
            df_train = pd.concat([df_train, df_train_org.loc[indicies]], ignore_index=True)

    builded_model = make_model(model_name, num_classes=n_classes, pretrained=True, dropout_p=dropout)
    if channel_mode == "rgby":
        if model_name == "inception_v4":
            builded_model._features[0].conv = Conv2d(4, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)

        if model_name == "pnasnet5large":
            builded_model._features.conv_0.conv = Conv2d(4, 96, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name == "polynet":
            builded_model._features[0].conv1[0].conv = Conv2d(4, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name in ['se_resnet50', 'se_resnet101', 'se_resnet152', 'se_resnext101_32x4d', 'se_resnext50_32x4d',
                          'se_resnext101_64x4d']:
            builded_model._features[0].conv1 = Conv2d(4, 64,
                                                      kernel_size=(first_filter, first_filter),
                                                      stride=(2, 2),
                                                      padding=(first_padding, first_padding), bias=False)
        if model_name in ["resnet18", "resnet34"]:
            builded_model._features[0] = Conv2d(4, 64,
                                                kernel_size=(first_filter, first_filter),
                                                stride=(2, 2),
                                                padding=(first_padding, first_padding), bias=False)

    builded_model.cuda()
    # builded_model.half()

    """
    train_mean = [0.08069, 0.05258, 0.05487, 0.08282]
    train_std = [0.13704, 0.10145, 0.15313, 0.13814]
    if channel_mode == "rgb":
        train_mean = train_mean[:3]
        train_std = train_std[:3]
    """
    # composed = transforms.Compose([transforms.RandomHorizontalFlip(),
    #                                transforms.RandomRotation(30),
    #                                transforms.Normalize(train_mean, train_std)])
    # composed = transforms.Compose([])
    # composed = transforms.Compose([RandomHorizontalFlip(0.5),
    #                                RandomVerticallFlip(0.5)])
    composed = albumentations.Compose(composed_ary)
    train_dataset = ProteinDataset(df_images=df_train,
                                   is_train=True,
                                   channel_mode=channel_mode,
                                   shape=shape,
                                   transform=composed)
    validation_dataset = ProteinDataset(df_images=df_test,
                                        is_train=True,
                                        shape=shape,
                                        channel_mode=channel_mode)

    if use_random_sampler:
        sampler = get_weighted_sampler(df_train, gamma=random_sampler_gamma)
        shuffle = False
    else:
        sampler = None
        shuffle = True

    def worker_init_fn(worker_id):
        return np.random.seed(np.random.get_state()[1][0] + worker_id)

    train_dataloader = DataLoader(train_dataset,
                                  batch_size=batch_size,
                                  shuffle=shuffle,
                                  sampler=sampler,
                                  num_workers=4,
                                  worker_init_fn=worker_init_fn)
    validation_dataloader = DataLoader(validation_dataset,
                                       batch_size=batch_size,
                                       shuffle=False,
                                       num_workers=4)

    model = Model(builded_model)
    # ----------------------
    # 1. Training new layer
    # ----------------------
    def change_freeze(model, is_freeze):
        for param in model.parameters():
            param.requires_grad = not is_freeze
        # NN変更部分はfreezeしない
        for param in model._features[0].parameters():
            param.requires_grad = True
        for param in model._classifier.parameters():
            param.requires_grad = True


    if epoch_1st > 0:
        logger.debug("** 1. Training new layer ** ")
        # loss = nn.BCEWithLogitsLoss()
        loss = FocalLoss(gamma=2)
        if optimizer == "sgd":
            optimizer = optim.SGD(builded_model.parameters(), **optimizer_param)
        if optimizer == "adam":
            optimizer = optim.Adam(builded_model.parameters(), **optimizer_param)
        # scheduler = StepLR(optimizer, step_size=step_size, gamma=gamma)
        scheduler_batch = CyclicLR(optimizer=optimizer, **cycliclr_param)
        scheduler = ReduceLROnPlateau(optimizer=optimizer, mode="min", factor=0.1, patience=1, verbose=True)
        metrics = F_score()
        checkpoint = Checkpoint(filepath="{}/best_1.pth".format(output_dir),
                                monitor="metrics_val",
                                mode="max",
                                logger=logger)

        change_freeze(model.model, is_freeze=True)
        logger.info(builded_model)
        logger.info(loss)
        logger.info(optimizer)
        # logger.info(scheduler)
        logger.info(composed)

        model.fit(train_dataloader=train_dataloader,
                  validation_dataloader=validation_dataloader,
                  epoch=epoch_1st,
                  loss=loss,
                  optimizer=optimizer,
                  scheduler=scheduler,
                  scheduler_batch=scheduler_batch,
                  metrics=metrics,
                  logger=logger,
                  callbacks=[checkpoint])
        # model.model.load_state_dict(torch.load("{}/best_1".format(output_dir)))

    # ----------------------
    # 2. Training all layer
    # ---------------------
    logger.debug("** 2. Training all layer ** ")
    # loss = nn.BCEWithLogitsLoss()
    loss = FocalLoss(gamma=2)
    if optimizer == "sgd":
        optimizer = optim.SGD(builded_model.parameters(), **optimizer_param)
    if optimizer == "adam":
        optimizer = optim.Adam(builded_model.parameters(), **optimizer_param)
    # scheduler = StepLR(optimizer, step_size=step_size, gamma=gamma)
    # scheduler_batch = CyclicLR(optimizer=optimizer, base_lr=optimizer_param["lr"], max_lr=optimizer_param["lr"]*2,
    #                            step_size=300, mode="exp_range", gamma=0.99994)
    scheduler_batch = CyclicLR(optimizer=optimizer, **cycliclr_param)
    scheduler = ReduceLROnPlateau(optimizer=optimizer, mode="min", factor=0.1, patience=1, verbose=True)
    metrics = F_score()
    checkpoint = Checkpoint(filepath="{}/best_2".format(output_dir),
                            monitor="metrics_val",
                            mode="max",
                            logger=logger)
    change_freeze(model.model, is_freeze=False)
    logger.info(builded_model)
    logger.info(loss)
    logger.info(optimizer)
    logger.info(scheduler)
    logger.info(composed_ary)
    model.fit(train_dataloader=train_dataloader,
              validation_dataloader=validation_dataloader,
              epoch=epoch_2nd,
              loss=loss,
              optimizer=optimizer,
              scheduler=scheduler,
              scheduler_batch=scheduler_batch,
              metrics=metrics,
              logger=logger,
              callbacks=[checkpoint])

    # ----------------------
    # 3. Calculate best threshold
    # ----------------------
    _, val_metrics = model.evaluate(validation_dataloader, loss_func=loss, metrics=metrics)
    with open("{}/val_metrics.pickle".format(output_dir), "wb") as f:
        pickle.dump(val_metrics, f)
    logger.info("best threshold: \n{}".format(val_metrics.result))
    ###########################################
    # submit
    ###########################################
    def tta(df, composed, enable_dropout=False):
        sub_dataset = ProteinDataset(df_images=df,
                                     is_train=False,
                                     shape=shape,
                                     channel_mode=channel_mode,
                                     transform=composed)

        logger.debug("prediction...")
        pred = model.predict(sub_dataset, enable_dropout=enable_dropout)
        return pred
    model.model.load_state_dict(torch.load("{}/best_2.pth".format(output_dir)))
    logger.debug("Submit start")

    df_sub = pd.read_csv("../../data/original/sample_submission.csv")
    if is_debug:
        df_sub = df_sub.iloc[:300]

    # TTA
    sub_pred = []
    """
    for cp in composed_ary:
        cp.p = 1
        sub_pred.append(tta(df=df_sub, composed=albumentations.Compose([cp])))
    """
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.HorizontalFlip(p=1)]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.VerticalFlip(p=1)]),
                        enable_dropout=False))
    sub_pred.append(tta(df=df_sub,
                        composed=albumentations.Compose([albumentations.VerticalFlip(p=1),
                                                         albumentations.HorizontalFlip(p=1)]),
                        enable_dropout=False))
    with open("{}/submission_detail.numpy.pickle".format(output_dir), "wb") as f:
        pickle.dump(sub_pred, f)

    sub_pred = np.array(sub_pred).max(axis=0)

    logger.debug("make submit file")
    sub = []
    for i in range(len(val_metrics.result)):
        w_sub = (sub_pred[:, i] > val_metrics.result.iloc[i]["threshold"]).astype(int)
        sub.append(w_sub)

    sub = np.array(sub).transpose((1, 0))

    def make_submission_file(sample_submission_df, predictions):
        submissions = []
        for row in predictions:
            subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
            submissions.append(subrow)

        sample_submission_df['Predicted'] = submissions
        sample_submission_df.to_csv('{}/submission.csv'.format(output_dir), index=None)

    make_submission_file(df_sub, sub)

    s_handler.close()
    f_handler.close()
    logger.handlers = []
    del logger, s_handler, f_handler, formatter
    gc.collect()

if __name__ == "__main__":
    # print("sleep")
    # time.sleep(60*60*2)
    shape = 512
    composed = [albumentations.ElasticTransform(p=0.5),
                albumentations.HorizontalFlip(p=0.5),
                albumentations.VerticalFlip(p=0.5),
                albumentations.RandomSizedCrop(min_max_height=(int(shape*0.25), int(shape*0.75)), width=shape, height=shape, p=0.5)
                ]

    # run(shape=shape, epoch_1st=0, epoch_2nd=25, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
    #     optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.0001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=False, target_fold=0,
    #     n_splits=5, dropout=0.1, aug_minor_class=True, cycliclr_param={"base_lr": 0.0001, "max_lr": 0.0005, "step_size": 300, "mode": "triangular2", "gamma": 0.99994})
    # run(shape=shape, epoch_1st=0, epoch_2nd=25, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
    #     optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.0001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=False, target_fold=1,
    #     n_splits=5, dropout=0.1, aug_minor_class=True, cycliclr_param={"base_lr": 0.0001, "max_lr": 0.0005, "step_size": 300, "mode": "triangular2", "gamma": 0.99994})
    run(shape=shape, epoch_1st=0, epoch_2nd=25, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
        optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.0001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=False, target_fold=2,
        n_splits=5, dropout=0.1, aug_minor_class=True, cycliclr_param={"base_lr": 0.0001, "max_lr": 0.0005, "step_size": 300, "mode": "triangular2", "gamma": 0.99994})
    run(shape=shape, epoch_1st=0, epoch_2nd=25, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
        optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.0001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=False, target_fold=3,
        n_splits=5, dropout=0.1, aug_minor_class=True, cycliclr_param={"base_lr": 0.0001, "max_lr": 0.0005, "step_size": 300, "mode": "triangular2", "gamma": 0.99994})
    run(shape=shape, epoch_1st=0, epoch_2nd=25, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
        optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.0001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=False, target_fold=4,
        n_splits=5, dropout=0.1, aug_minor_class=True, cycliclr_param={"base_lr": 0.0001, "max_lr": 0.0005, "step_size": 300, "mode": "triangular2", "gamma": 0.99994})
