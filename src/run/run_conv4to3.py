from src.net.resnet34_4to3 import Resnet34
from src.common_lib import *

def run():

    batch_size = 16
    epoch_1st = 5
    epoch_2nd = 50
    is_debug = True
    ###########################################
    # pre_process
    ###########################################
    output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d_%H%M%S"))

    # config logger
    os.makedirs(output_dir)
    logger = getLogger("run.py")
    formatter = Formatter("%(asctime)s :%(message)s")

    s_handler = StreamHandler()
    s_handler.setLevel(DEBUG)
    s_handler.setFormatter(formatter)
    f_handler = FileHandler(filename="{}/log.text".format(output_dir))
    f_handler.setLevel(INFO)
    f_handler.setFormatter(formatter)
    logger.setLevel(DEBUG)
    logger.addHandler(s_handler)
    logger.addHandler(f_handler)

    # torch config
    torch.backends.cudnn.benchmark = True
    ###########################################
    # training
    ###########################################
    logger.debug("Training start")
    df = pd.read_csv("../../data/original/train.csv")
    if is_debug:
        logger.debug("DEBUG MODE")
        df = df.iloc[:50] # test
        epoch_1st = 3
        epoch_2nd = 2
    fold = KFold(n_splits=3, random_state=0, shuffle=True)


    for train_idx, test_idx in fold.split(df):
        break

    df_train = df.iloc[train_idx]
    df_test = df.iloc[test_idx]

    resnet = Resnet34()
    resnet.cuda()
    train_dataset = ProteinDataset(df_images=df_train,
                                   is_train=True,
                                   channel_mode="rgby")
    validation_dataset = ProteinDataset(df_images=df_test,
                                        is_train=True,
                                        channel_mode="rgby")

    train_dataloader = DataLoader(train_dataset,
                                  batch_size=batch_size,
                                  shuffle=True)
    validation_dataloader = DataLoader(validation_dataset,
                                       batch_size=batch_size,
                                       shuffle=False)

    model = Model(resnet)
    # ----------------------
    # 1. Training new layer
    # ----------------------

    logger.debug("** 1. Training new layer ** ")
    loss = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(resnet.parameters(), lr=0.01)
    metrics = F_score()
    checkpoint = Checkpoint(filepath="{}/best_1.pth".format(output_dir),
                            monitor="metrics_val",
                            mode="max",
                            logger=logger)

    model.model.change_freeze(is_freeze=True)
    model.fit(train_dataloader=train_dataloader,
              validation_dataloader=validation_dataloader,
              epoch=epoch_1st,
              loss=loss,
              optimizer=optimizer,
              metrics=metrics,
              logger=logger,
              callbacks=[checkpoint])
    # ----------------------
    # 2. Training all layer
    # ----------------------

    logger.debug("** 2. Training all layer ** ")
    model.model.load_state_dict(torch.load("{}/best_1.pth".format(output_dir)))
    loss = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(resnet.parameters(), lr=0.0001)
    metrics = F_score()
    checkpoint = Checkpoint(filepath="{}/best_2.pth".format(output_dir),
                            monitor="metrics_val",
                            mode="max",
                            logger=logger)
    model.model.change_freeze(is_freeze=False)
    model.fit(train_dataloader=train_dataloader,
              validation_dataloader=validation_dataloader,
              epoch=epoch_2nd,
              loss=loss,
              optimizer=optimizer,
              metrics=metrics,
              logger=logger,
              callbacks=[checkpoint])

    # ----------------------
    # 3. Calculate best threshold
    # ----------------------
    _, val_metrics = model.evaluate(validation_dataloader, loss_func=loss, metrics=metrics)
    logger.debug("best threshold: \n{}".format(val_metrics.result))
    ###########################################
    # submit
    ###########################################
    model.model.load_state_dict(torch.load("{}/best_2.pth".format(output_dir)))
    logger.debug("Submit start")
    df_sub = pd.read_csv("../../data/original/sample_submission.csv")
    if is_debug:
        df_sub = df_sub.iloc[:300]
    sub_dataset = ProteinDataset(df_images=df_sub,
                                 is_train=False,
                                 channel_mode="rgby")

    logger.debug("prediction...")
    sub_pred = model.predict(sub_dataset)

    logger.debug("make submit file")
    sub = []
    for i in range(len(val_metrics.result)):
        w_sub = (sub_pred[:, i] > val_metrics.result.iloc[i]["threshold"]).astype(int)
        sub.append(w_sub)

    sub = np.array(sub).transpose((1, 0))

    def make_submission_file(sample_submission_df, predictions):
        submissions = []
        for row in predictions:
            subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
            submissions.append(subrow)

        sample_submission_df['Predicted'] = submissions
        sample_submission_df.to_csv('{}/submission.csv'.format(output_dir), index=None)

    make_submission_file(df_sub, sub)

if __name__ == "__main__":
    run()
