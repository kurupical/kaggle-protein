from src.net.resnet34 import Resnet34
from src.net.resnet101 import Resnet101
from src.common_lib import *

def run(shape, model_name, epoch_1st, epoch_2nd, batch_size, step_size, gamma, channel_mode,
        optimizer, optimizer_param, composed_ary, is_debug=False, first_filter=7, first_padding=3,
        use_external_data=True, use_random_sampler=True, random_sampler_gamma=2.0, fold=0, weight_path=""):

    # https://github.com/creafz/pytorch-cnn-finetune
    seed = 0
    random.seed(seed)
    torch.manual_seed(seed)

    n_splits = 5
    n_classes = 28
    ###########################################
    # pre_process
    ###########################################
    output_dir = "../../output/{}".format(dt.now().strftime("%Y%m%d_%H%M%S"))


    # config logger
    os.makedirs(output_dir)
    logger = getLogger("run.py")
    formatter = Formatter("%(asctime)s :%(message)s")

    s_handler = StreamHandler()
    s_handler.setLevel(DEBUG)
    s_handler.setFormatter(formatter)
    f_handler = FileHandler(filename="{}/log.text".format(output_dir))
    f_handler.setLevel(INFO)
    f_handler.setFormatter(formatter)
    logger.setLevel(DEBUG)
    logger.addHandler(s_handler)
    logger.addHandler(f_handler)

    # torch config
    torch.backends.cudnn.benchmark = True
    ###########################################
    # training
    ###########################################
    logger.debug("Training start")
    df = pd.read_csv("../../data/original/train.csv")
    if use_external_data:
        df = df.append(pd.read_csv("../../data/external/HPAv18/HPAv18RBGY_wodpl_smallclass.csv"))
    if is_debug:
        logger.debug("DEBUG MODE")
        df = df.iloc[:60] # test
        epoch_1st = 10
        epoch_2nd = 10
    fold = KFold(n_splits=n_splits, random_state=seed, shuffle=True)


    i = 0
    for train_idx, test_idx in fold.split(df):
        if fold == i:
            break
        i += 1

    df_train = df.iloc[train_idx]
    df_test = df.iloc[test_idx]

    builded_model = make_model(model_name, num_classes=n_classes, pretrained=True, dropout_p=0.5)
    if channel_mode == "rgby":
        if model_name == "inception_v4":
            builded_model._features[0].conv = Conv2d(4, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)

        if model_name == "pnasnet5large":
            builded_model._features.conv_0.conv = Conv2d(4, 96, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name == "polynet":
            builded_model._features[0].conv1[0].conv = Conv2d(4, 32, kernel_size=(3, 3), stride=(2, 2), bias=False)
        if model_name in ['se_resnet50', 'se_resnet101', 'se_resnet152', 'se_resnext101_32x4d', 'se_resnext50_32x4d',
                          'se_resnext101_64x4d']:
            builded_model._features[0].conv1 = Conv2d(4, 64,
                                                      kernel_size=(first_filter, first_filter),
                                                      stride=(2, 2),
                                                      padding=(first_padding, first_padding), bias=False)
    builded_model.cuda()
    # builded_model.half()

    """
    train_mean = [0.08069, 0.05258, 0.05487, 0.08282]
    train_std = [0.13704, 0.10145, 0.15313, 0.13814]
    if channel_mode == "rgb":
        train_mean = train_mean[:3]
        train_std = train_std[:3]
    """
    # composed = transforms.Compose([transforms.RandomHorizontalFlip(),
    #                                transforms.RandomRotation(30),
    #                                transforms.Normalize(train_mean, train_std)])
    # composed = transforms.Compose([])
    # composed = transforms.Compose([RandomHorizontalFlip(0.5),
    #                                RandomVerticallFlip(0.5)])
    composed = albumentations.Compose(composed_ary)
    train_dataset = ProteinDataset(df_images=df_train,
                                   is_train=True,
                                   channel_mode=channel_mode,
                                   shape=shape,
                                   transform=composed)
    validation_dataset = ProteinDataset(df_images=df_test,
                                        is_train=True,
                                        shape=shape,
                                        channel_mode=channel_mode)

    if use_random_sampler:
        sampler = get_weighted_sampler(df_train, gamma=random_sampler_gamma)
        shuffle = False
    else:
        sampler = None
        shuffle = True
    train_dataloader = DataLoader(train_dataset,
                                  batch_size=batch_size,
                                  shuffle=shuffle,
                                  sampler=sampler,
                                  num_workers=1)
    validation_dataloader = DataLoader(validation_dataset,
                                       batch_size=batch_size,
                                       shuffle=False,
                                       num_workers=1)

    model = Model(builded_model)

    # ----------------------
    # 1. Training new layer
    # ----------------------
    def change_freeze(model, is_freeze):
        for param in model.parameters():
            param.requires_grad = not is_freeze
        # NN変更部分はfreezeしない
        for param in model._features[0].parameters():
            param.requires_grad = True
        for param in model._classifier.parameters():
            param.requires_grad = True


    """
    if epoch_1st > 0:
        logger.debug("** 1. Training new layer ** ")
        # loss = nn.BCEWithLogitsLoss()
        loss = FocalLoss(gamma=2)
        if optimizer == "sgd":
            optimizer = optim.SGD(builded_model.parameters(), **optimizer_param)
        if optimizer == "adam":
            optimizer = optim.Adam(builded_model.parameters(), **optimizer_param)
        # scheduler = StepLR(optimizer, step_size=step_size, gamma=gamma)
        scheduler_batch = CyclicLR(optimizer=optimizer, base_lr=optimizer_param["lr"], max_lr=optimizer_param["lr"] * 5,
                                   step_size=300, mode="triangular2")
        scheduler = ReduceLROnPlateau(optimizer=optimizer, mode="min", factor=0.1, patience=1, verbose=True)
        metrics = F_score()
        checkpoint = Checkpoint(filepath="{}/best_1.pth".format(output_dir),
                                monitor="metrics_val",
                                mode="max",
                                logger=logger)

        change_freeze(model.model, is_freeze=True)
        logger.info(builded_model)
        logger.info(loss)
        logger.info(optimizer)
        # logger.info(scheduler)
        logger.info(composed)

        model.fit(train_dataloader=train_dataloader,
                  validation_dataloader=validation_dataloader,
                  epoch=epoch_1st,
                  loss=loss,
                  optimizer=optimizer,
                  scheduler=scheduler,
                  scheduler_batch=scheduler_batch,
                  metrics=metrics,
                  logger=logger,
                  callbacks=[checkpoint])

    """
    model.model.load_state_dict(torch.load(weight_path))

    # ----------------------
    # 2. Training all layer
    # ---------------------
    logger.debug("** 2. Training all layer ** ")
    # loss = nn.BCEWithLogitsLoss()
    loss = FocalLoss(gamma=2)
    if optimizer == "sgd":
        optimizer = optim.SGD(builded_model.parameters(), **optimizer_param)
    if optimizer == "adam":
        optimizer = optim.Adam(builded_model.parameters(), **optimizer_param)
    # scheduler = StepLR(optimizer, step_size=step_size, gamma=gamma)
    # scheduler_batch = CyclicLR(optimizer=optimizer, base_lr=optimizer_param["lr"], max_lr=optimizer_param["lr"]*2,
    #                            step_size=300, mode="exp_range", gamma=0.99994)
    scheduler_batch = CyclicLR(optimizer=optimizer, base_lr=optimizer_param["lr"], max_lr=optimizer_param["lr"]*5,
                               step_size=300, mode="triangular2")
    scheduler = ReduceLROnPlateau(optimizer=optimizer, mode="min", factor=0.1, patience=1, verbose=True)
    metrics = F_score()
    checkpoint = Checkpoint(filepath="{}/best_2".format(output_dir),
                            monitor="metrics_val",
                            mode="max",
                            logger=logger)
    change_freeze(model.model, is_freeze=False)
    logger.info(builded_model)
    logger.info(loss)
    logger.info(optimizer)
    logger.info(scheduler)
    logger.info(composed_ary)
    model.fit(train_dataloader=train_dataloader,
              validation_dataloader=validation_dataloader,
              epoch=epoch_2nd,
              loss=loss,
              optimizer=optimizer,
              scheduler=scheduler,
              scheduler_batch=scheduler_batch,
              metrics=metrics,
              logger=logger,
              callbacks=[checkpoint])

    # ----------------------
    # 3. Calculate best threshold
    # ----------------------
    _, val_metrics = model.evaluate(validation_dataloader, loss_func=loss, metrics=metrics)
    with open("{}/val_metrics.pickle".format(output_dir), "wb") as f:
        pickle.dump(val_metrics, f)
    logger.info("best threshold: \n{}".format(val_metrics.result))
    ###########################################
    # submit
    ###########################################
    def tta(df, composed):
        sub_dataset = ProteinDataset(df_images=df,
                                     is_train=False,
                                     shape=shape,
                                     channel_mode=channel_mode,
                                     transform=composed)

        logger.debug("prediction...")
        pred = model.predict(sub_dataset)
        return pred
    # model.model.load_state_dict(torch.load("{}/best_2.pth".format(output_dir)))
    logger.debug("Submit start")

    df_sub = pd.read_csv("../../data/original/sample_submission.csv")
    if is_debug:
        df_sub = df_sub.iloc[:300]

    # TTA
    sub_pred = []
    sub_pred.append(tta(df=df_sub, composed=albumentations.Compose([])))
    for cp in composed_ary:
        cp.p = 1
        sub_pred.append(tta(df=df_sub, composed=albumentations.Compose([cp])))

    sub_pred = np.array(sub_pred).max(axis=0)

    logger.debug("make submit file")
    sub = []
    for i in range(len(val_metrics.result)):
        w_sub = (sub_pred[:, i] > val_metrics.result.iloc[i]["threshold"]).astype(int)
        sub.append(w_sub)

    sub = np.array(sub).transpose((1, 0))

    def make_submission_file(sample_submission_df, predictions):
        submissions = []
        for row in predictions:
            subrow = ' '.join(list([str(i) for i in np.nonzero(row)[0]]))
            submissions.append(subrow)

        sample_submission_df['Predicted'] = submissions
        sample_submission_df.to_csv('{}/submission.csv'.format(output_dir), index=None)

    make_submission_file(df_sub, sub)

    s_handler.close()
    f_handler.close()
    logger.handlers = []
    del logger, s_handler, f_handler, formatter, model, builded_model
    gc.collect()

if __name__ == "__main__":
    # print("sleep")
    # time.sleep(60*60*1)
    shape = 512
    composed = [albumentations.ElasticTransform(p=0.25),
                albumentations.HorizontalFlip(p=0.15),
                albumentations.RandomSizedCrop(min_max_height=(int(shape*0.25), int(shape*0.75)), width=shape, height=shape, p=0.15),
                ChannelShuffle_4dim(p=0.05)]
    run(shape=shape, epoch_1st=0, epoch_2nd=1, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
        optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.00001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=False, fold=0,
        weight_path="../../submit/20181216_512*512ext/20181215_141508_fold0/best_2_epoch20.pth")
    run(shape=shape, epoch_1st=0, epoch_2nd=1, batch_size=6, model_name="se_resnext50_32x4d", is_debug=False, step_size="nothing", gamma="nothing", channel_mode="rgby",
        optimizer="adam", first_filter=7, first_padding=3, optimizer_param={"lr": 0.00001}, composed_ary=composed, random_sampler_gamma=0.5, use_random_sampler=True, fold=0,
        weight_path="../../submit/20181216_512*512ext/20181215_141508_fold0/best_2_epoch20.pth")
