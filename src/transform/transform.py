from src.common_lib import *
from albumentations.core.transforms_interface import ImageOnlyTransform

class RandomHorizontalFlip(object):

    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, img):
        if random.random() < self.p:
            return np.fliplr(img)
        return img

    def __repr__(self):
        return self.__class__.__name__ + '(p={})'.format(self.p)

class RandomVerticallFlip(object):

    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, img):
        if random.random() < self.p:
            return np.flipud(img)
        return img

    def __repr__(self):
        return self.__class__.__name__ + '(p={})'.format(self.p)


class Normalize(object):
    """Normalize an tensor image with mean and standard deviation.

    Given mean: (R, G, B) and std: (R, G, B),
    will normalize each channel of the torch.*Tensor, i.e.
    channel = (channel - mean) / std

    Args:
        mean (sequence): Sequence of means for R, G, B channels respecitvely.
        std (sequence): Sequence of standard deviations for R, G, B channels
            respecitvely.
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, img):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.

        Returns:
            Tensor: Normalized image.
        """
        for i in range(len(img)):
            img[i, :, :] = (img[i, :, :] - np.array(self.mean[i])) / np.array(self.std[i])
        return img

def channel_shuffle(img):
    ch_arr = [0, 1, 2, 3]
    random.shuffle(ch_arr)
    img = img[..., ch_arr]
    return img

class ChannelShuffle_4dim(ImageOnlyTransform):
    """Randomly rearrange channels of the input RGB image.

    Args:
        p (float): probability of applying the transform. Default: 0.5.

    Targets:
        image

    Image types:
        uint8, float32
    """

    def apply(self, img, **params):
        return channel_shuffle(img)

class Rotate90(ImageOnlyTransform):

    def apply(self, img, **params):
        return np.rot90(img, 1, axes=(0, 1))

class Rotate270(ImageOnlyTransform):

    def apply(self, img, **params):
        return np.rot90(img, 3, axes=(0, 1))
