
from src.base.callback import Callback
from src.common_lib import *

class Checkpoint(Callback):
    def __init__(self, filepath, monitor, mode, logger):
        super(Checkpoint, self).__init__()
        self.filepath = filepath
        self.monitor = monitor
        self.logger = logger
        if mode not in ["max", "min"]:
            raise ValueError("invalid mode. mode should be ['max', 'min']")
        if mode == "max":
            self.monitor_op = np.less
            self.best = -np.inf
        if mode == "min":
            self.monitor_op = np.greater
            self.best = np.inf

    def on_epoch_end(self, params=None):
        if self.monitor_op(self.best, params[self.monitor]):
            self.logger.debug("model improved {} to {}".format(self.best, params[self.monitor]))
            self.best = params[self.monitor]
            torch.save(self.model.state_dict(), "{}.pth".format(self.filepath))
            torch.save(self.model.state_dict(), "{}_epoch{}.pth".format(self.filepath, params["epoch"]))