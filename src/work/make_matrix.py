from src.common_lib import *

models = (['resnext101_32x4d', 'resnext101_64x4d'] +
          ['inceptionresnetv2'] +
          # ['dpn68', 'dpn92', 'dpn98', 'dpn131', 'dpn107'] +
          ['inception_v4'] +
          # ['xception'] +
          ['se_resnet50', 'se_resnet101', 'se_resnet152', 'se_resnext101_32x4d'] +
          ['pnasnet5large'] +
          ['polynet'])

for model_names in models:
    make_model(model_names, num_classes=28, pretrained=True, dropout_p=0.5)