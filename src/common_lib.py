from torchvision.models import resnet18, resnet34, resnet101
from torchvision import transforms
import torch
import torch.nn as nn
import torch.optim as optim

from torch.autograd import Variable
from torch.optim.lr_scheduler import ReduceLROnPlateau, CosineAnnealingLR, StepLR
from torch.nn import Conv2d
import torch.nn.functional as F
# from torchvision.transforms import Resize
from sklearn.preprocessing import MultiLabelBinarizer
from torch.utils.data import Dataset, DataLoader
from PIL import Image
import glob
import tqdm
import gc
import random

import numpy as np
import pandas as pd
from sklearn.model_selection import KFold
import copy
from logging import getLogger
from datetime import datetime as dt
from logging import getLogger, StreamHandler, FileHandler, DEBUG, INFO, Formatter
import os
import time
from tqdm import tqdm
import pickle
import cv2

from src.metrics.f_score import F_score
from src.callback.checkpoint import Checkpoint
from src.dataloader.protein_dataloader import *
from src.meta.model import Model
from src.loss.loss import *
from src.transform.transform import *
import scipy.optimize as opt

from src.common import *

from cnn_finetune import make_model

from fastai.vision.transform import *
import albumentations