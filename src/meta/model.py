from src.common_lib import *

class Model:
    def __init__(self, model, is_gpu=True, dtype="float32", is_half=False):
        self.model = model
        self.is_gpu = is_gpu
        self.dtype = dtype
        self.is_half = is_half
        self.result_dicts = []

    def fit(self, train_dataloader, validation_dataloader,
            epoch, loss, metrics, logger, callbacks=[], optimizer=None, scheduler=None, scheduler_batch=None):

        """

        :param train_dataset:
        :param validation_dataset:
        :param epoch:
        :param batch_size:
        :param loss: Function
        :param optimizer: Function
        :param metrics: Function
        :param logger: logger
            * logger.debug : output to [console, log_file]
            * logger.info : output to [log_file]
        :param checkpoint: Function
        :return:
        """

        for callback in callbacks:
            callback.set_model(self.model)
            callback.on_batch_begin()

        for epoch in np.arange(1, epoch+1):
            for callback in callbacks:
                callback.on_epoch_begin()
            start_time = time.time()
            loss_train, metrics_train = self._train(train_dataloader, loss, optimizer, scheduler_batch, copy.deepcopy(metrics))
            # loss_train, metrics_train = self._evaluate(train_dataloader, loss, copy.deepcopy(metrics))
            loss_val, metrics_val = self.evaluate(validation_dataloader, loss, copy.deepcopy(metrics))
            # scheduler.step(loss_val)
            decay_lr_ratio = 1
            if not scheduler is None:
                lr_before = optimizer.param_groups[0]["lr"]
                if type(scheduler) == ReduceLROnPlateau:
                    scheduler.step(loss_val)
                    lr_after = optimizer.param_groups[0]["lr"]
                if type(scheduler) == StepLR:
                    scheduler.step()
                    lr_after = optimizer.param_groups[0]["lr"]
                decay_lr_ratio = lr_after / lr_before
            if not scheduler_batch is None:
                scheduler_batch.base_lrs = [x*decay_lr_ratio for x in scheduler_batch.base_lrs]
                scheduler_batch.max_lrs = [x*decay_lr_ratio for x in scheduler_batch.max_lrs]
            time_spent = int(time.time() - start_time)
            self._output_log(loss_train, loss_val, metrics_train, metrics_val, epoch, time_spent, logger)

            result_dict = {
                "epoch": epoch,
                "loss_train": loss_train,
                "loss_val": loss_val,
                "metrics_train": metrics_train.score,
                "metrics_val": metrics_val.score,
                "time_spent": time_spent,
                "logger": logger
            }
            self.result_dicts.append(result_dict)
            for callback in callbacks:
                callback.on_epoch_end(result_dict)
            print(optimizer)
        for callback in callbacks:
            callback.on_batch_end()

    def predict(self, dataset, enable_dropout=False):
        dataloader = DataLoader(dataset,
                                batch_size=8,
                                shuffle=False,
                                num_workers=4)

        y_pred = []
        batch_size = dataloader.batch_size
        with torch.no_grad():
            if not enable_dropout:
                self.model.eval()
            else:
                self.model.train()
            with tqdm(range(len(dataloader) * batch_size), desc="prediction") as pbar:
                for data_batched in dataloader:
                    X = data_batched

                    if self.is_gpu:
                        X = X.cuda()
                    if self.is_half:
                        X = X.half()

                    pred = self.model(X)
                    y_pred.extend([x for x in pred.cpu().numpy().tolist()])
                    pbar.update(batch_size)

        y_pred = np.array(y_pred)

        return y_pred

    def _train(self, dataloader, loss_func, optimizer, scheduler_batch, metrics):
        self.model.train()
        total_loss = 0.0
        batch_size = dataloader.batch_size
        y_pred = []
        y_true = []
        self.model.train()
        with tqdm(range(len(dataloader) * batch_size), desc="training") as pbar:
            for data_batched in dataloader:
                X, y = data_batched

                if self.is_gpu:
                    X = X.cuda()
                    y = y.cuda()
                if self.is_half:
                    X = X.half()
                    y = y.half()

                optimizer.zero_grad()
                pred = self.model(X)
                loss = loss_func(pred, y)
                loss.backward()
                optimizer.step()

                total_loss += float(loss)
                y_pred.extend([x for x in pred.cpu().detach().numpy().tolist()])
                y_true.extend([x for x in y.cpu().detach().numpy().tolist()])

                pbar.update(batch_size)
                if not scheduler_batch is None:
                    scheduler_batch.batch_step()
                # print(optimizer)
                del X, y
                gc.collect()
        y_pred = np.array(y_pred)
        y_true = np.array(y_true)

        total_loss = total_loss / (len(y_pred) / batch_size)
        metrics.calc_score(y_true, y_pred)

        return total_loss, metrics

    def evaluate(self, dataloader, loss_func, metrics):
        with torch.no_grad():
            self.model.eval()
            y_pred, y_true = self._predict_to_eval(dataloader)
            total_loss = loss_func(torch.from_numpy(y_pred), torch.from_numpy(y_true))
            metrics.calc_score(y_true, y_pred)
            return total_loss, metrics

    def _predict_to_eval(self, dataloader):
        y_pred = []
        y_true = []
        batch_size = dataloader.batch_size
        self.model.eval()
        with tqdm(range(len(dataloader) * batch_size), desc="predict_to_eval") as pbar:
            for data_batched in dataloader:
                X, y = data_batched

                if self.is_gpu:
                    X = X.cuda()
                    y = y.cuda()
                if self.is_half:
                    X = X.half()
                    y = y.half()

                pred = self.model(X)
                y_pred.extend([x for x in pred.cpu().numpy().tolist()])
                y_true.extend([x for x in y.cpu().numpy().tolist()])

                pbar.update(batch_size)
        y_pred = np.array(y_pred)
        y_true = np.array(y_true)

        return y_pred, y_true

    def _output_log(self, loss_train, loss_val, metrics_train, metrics_val, epoch, time, logger, line_per_epoch=2):
        if line_per_epoch == 1:
            raise NotImplementedError
        if line_per_epoch == 2:
            return self._output_log_2line(loss_train, loss_val, metrics_train, metrics_val, epoch, time, logger)
        raise ValueError("Invalid parameter 'line_per_epoch'.")

    def _output_log_2line(self, loss_train, loss_val, metrics_train, metrics_val, epoch, time_spent, logger):
        def make_data_line(epoch, loss, metrics, display):
            data_row = ""
            if epoch is None:
                data_row += " " * 5 + "|"
            else:
                data_row += str(epoch).rjust(5) + "|"
            data_row += "{}s|".format(str(time_spent).rjust(4))
            data_row += str(display).rjust(5) + "|"
            data_row += "{:.4f}||".format(loss)
            data_row += metrics.log_data()

            return data_row

        if epoch == 1:
            # output header
            row_train_header_clsname, row_train_header_datanum = metrics_train.log_header()
            _, row_val_header_datanum = metrics_val.log_header()

            row_header_1 = "                        ||   cls   |" + row_train_header_clsname
            row_header_2 = "                        ||num|train|" + row_train_header_datanum
            row_header_3 = "                        ||   | val |" + row_val_header_datanum
            row_header_4 = "------------------------------------" + "-" * len(row_train_header_clsname)
            row_header_5 = "EPOCH| TIME| DATA| LOSS || F1micro |" + row_val_header_datanum
            row_header_6 = "====================================" + "=" * len(row_train_header_clsname)
            logger.info(row_header_1)
            logger.info(row_header_2)
            logger.info(row_header_3)
            logger.info(row_header_4)
            logger.info(row_header_5)
            logger.info(row_header_6)

        # make
        data_row_train = make_data_line(epoch=epoch, loss=loss_train, metrics=metrics_train, display="train")
        data_row_val = make_data_line(epoch=None, loss=loss_val, metrics=metrics_val, display=" val ")
        logger.info(data_row_train)
        logger.info(data_row_val)

        return True
