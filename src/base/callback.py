

class Callback:
    def __init__(self):
        pass

    def set_model(self, model):
        self.model = model

    def on_train_begin(self, params=None):
        pass

    def on_epoch_begin(self, params=None):
        pass

    def on_batch_begin(self, params=None):
        pass

    def on_epoch_end(self, params=None):
        pass

    def on_batch_end(self, params=None):
        pass
