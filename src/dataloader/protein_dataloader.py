from src.common_lib import *

LABEL_MAP = {
    0: "Nucleoplasm",
    1: "Nuclear membrane",
    2: "Nucleoli",
    3: "Nucleoli fibrillar center",
    4: "Nuclear speckles",
    5: "Nuclear bodies",
    6: "Endoplasmic reticulum",
    7: "Golgi apparatus",
    8: "Peroxisomes",
    9: "Endosomes",
    10: "Lysosomes",
    11: "Intermediate filaments",
    12: "Actin filaments",
    13: "Focal adhesion sites",
    14: "Microtubules",
    15: "Microtubule ends",
    16: "Cytokinetic bridge",
    17: "Mitotic spindle",
    18: "Microtubule organizing center",
    19: "Centrosome",
    20: "Lipid droplets",
    21: "Plasma membrane",
    22: "Cell junctions",
    23: "Mitochondria",
    24: "Aggresome",
    25: "Cytosol",
    26: "Cytoplasmic bodies",
    27: "Rods & rings"}
COLORS = ["_blue", "_green", "_red", "_yellow"]


class ProteinDataset(Dataset):
    def __init__(self, df_images, is_train,
                 root_dir="../../data/original",
                 external_dir="../../data/external/HPAv18/jpg",
                 channel_mode="rgb",
                 shape=512,
                 transform=None,
                 dtype=torch.float,
                 device=torch.device("cpu")):
        self.df_images = df_images
        self.is_train = is_train
        if is_train:
            self.root_dir = root_dir + "/train"
        else:
            self.root_dir = root_dir + "/test"
        self.external_dir = external_dir
        self.channel_mode = channel_mode
        self.shape = shape
        self.transform = transform
        self.dtype = dtype
        self.device = device
        self.choose_count = np.zeros(28)

    def __len__(self):
        return len(self.df_images)

    def __getitem__(self, idx):
        idx = int(idx)
        X = self._load_image(idx)
        y = None
        if self.is_train:
            label = self._load_multilabel_target(idx)
            y = np.zeros(28)
            for key in label:
                y[int(key)] = 1
            y = torch.from_numpy(y)
            y = y.to(device=self.device, dtype=self.dtype)

        if self.transform:
            X = X.transpose((1, 2, 0)) # To PIL Image
            X = self.transform(image=X)
            X = X["image"].transpose((2, 0, 1))  # To Normal Image
        X = torch.from_numpy(X.copy())
        X = X.to(device=self.device, dtype=self.dtype)

        if y is None:
            return X
        else:
            return X, y

    def _load_multilabel_target(self, idx):
        return list(map(int, self.df_images.iloc[idx].Target.split(' ')))

    def _load_image(self, idx):
        if self.channel_mode == "rgb":
            return self._load_image_rgb(idx)
        if self.channel_mode == "rgby":
            return self._load_image_rgby(idx)
        raise NotImplementedError

    def _load_image_rgb(self, idx):
        img_id = self.df_images["Id"].iloc[idx]
        ret_img = []
        for color in COLORS:
            if color == "_yellow":
                continue
            if len(img_id) == 36:
                # original data
                img_dir = self.root_dir + "/" + img_id + color + ".png"
            else:
                # external data
                img_dir = self.external_dir + "/" + img_id + color + ".jpg"
            img = Image.open(img_dir)
            img = img.resize((self.shape, self.shape), Image.BILINEAR)
            img = np.asarray(img)
            ret_img.append(img)

        ret_img = np.array(ret_img)
        assert ret_img.shape == (3, self.shape, self.shape)

        ret_img = ret_img / 255
        return ret_img

    def _load_image_rgby(self, idx):
        img_id = self.df_images["Id"].iloc[idx]
        ret_img = []
        for color in COLORS:
            if len(img_id) == 36:
                # original data
                img_dir = self.root_dir + "/" + img_id + color + ".png"
            else:
                # external data
                img_dir = self.external_dir + "/" + img_id + color + ".jpg"
            img = Image.open(img_dir)
            img = img.resize((self.shape, self.shape), Image.BILINEAR)
            img = np.asarray(img)
            if img.ndim == 3:
                # external data
                max_channel = np.argmax(img.sum(axis=(0, 1)))
                img = img[:, :, max_channel].reshape(self.shape, self.shape)
            ret_img.append(img)

        ret_img = np.array(ret_img)
        assert ret_img.shape == (4, self.shape, self.shape)

        ret_img = ret_img / 255
        return ret_img