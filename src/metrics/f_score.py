
from src.common_lib import *
from src.common import *
from sklearn.metrics import precision_recall_fscore_support

class F_score:

    def __init__(self):
        self.num_data_by_class = []

    def calc_score(self, y_true, y_pred):
        self.num_classes = len(y_true[0])

        self.result = pd.DataFrame(index=[],
                                   columns=["class_name", "threshold", "precision", "recall", "f_score"])

        for label_idx in range(self.num_classes):
            thresholds = logit(np.arange(0.35, 0.65, 0.01))

            # 全てのthresholdでf_scoreが0の場合は、best_thresholdは0.5とする
            best_f_score = 0
            best_precision = 0
            best_recall = 0
            best_threshold = logit(0.5) # 0

            true = y_true[:, label_idx]
            self.num_data_by_class.append(true.sum())
            for threshold in thresholds:
                pred = (y_pred[:, label_idx] > threshold).astype(int)
                precision, recall, f_score, _ = precision_recall_fscore_support(true, pred, average="binary")
                # print("class: {} / threshold: {} / f_score: {}".format(label_idx, threshold, f_score))
                if f_score == best_f_score:
                    best_f_score = f_score
                    best_precision = precision
                    best_recall = recall
                    # logit(0.5)に近いthresholdを採用する
                    if np.abs(threshold) < np.abs(best_threshold):
                        best_threshold = threshold

                if f_score > best_f_score:
                    best_f_score = f_score
                    best_precision = precision
                    best_recall = recall
                    best_threshold = threshold

            record = pd.Series([label_idx, best_threshold, best_precision, best_recall, best_f_score], index=self.result.columns)
            self.result = self.result.append(record, ignore_index=True)

        self.score = self.result["f_score"].mean()

    def log_header(self):
        row_1 = ""
        row_2 = ""
        for i in range(self.num_classes):
            row_1 += "cls{}|".format(str(i).rjust(3))
            row_2 += "{}|".format(str(self.num_data_by_class[i].astype(int)).rjust(6))
        return row_1, row_2

    def log_data(self):

        row = ""

        row += " {:.5f} |".format(self.score)
        for i in range(self.num_classes):
            w_result = self.result.query("class_name == {}".format(i))

            row += "{:.4f}|".format(w_result["f_score"].values[0])

        return row

